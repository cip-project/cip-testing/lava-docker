#!/bin/bash

print_help() {
	cat<<-EOF

	USAGE:
	        $0 [ARG]
	ARGs:
	        master  start master container
	        slave   start slave container
	        all     start both master and slave containers

	EOF
	exit 1
}

check_lava_configuration() {
	echo "Checking that the LAVA configuration is valid"

	# Check lavacli installation
	if ! command -v lavacli > /dev/null ; then
		echo "lavacli is not installed. Please install."
		exit 1
	fi
	# Check that the lavacli configuration is valid
	if ! lavacli system whoami > /dev/null ; then
		echo "lavacli is not configured correctly. Please check ~/.config/lavacli.yaml."
		exit 1
	fi
}
