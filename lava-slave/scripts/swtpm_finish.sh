#!/bin/sh
set -x

# Since we are not sure whether the job will be stopped or it will be finished with qemu poweorff
# We will kill it for sure in both cases.

# Since we start the tpm twice for swupdate related test cases, so this script will run twice in those
# cases which cannot be avoided. So we are checking for the presence of swtpm state directory and process
# before we try to kill them to avoid error scenarios

pstree -p | grep swtpm
if [ $? -eq 0 ]; then
        pkill -15 -x swtpm
fi

if [ -d /var/lib/lava/dispatcher/tmp/cip-core-security.swtpm ]; then
        rm -rf /var/lib/lava/dispatcher/tmp/cip-core-security.swtpm
fi

echo "Done"
